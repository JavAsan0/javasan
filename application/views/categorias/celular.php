<!-- 1 -->
<div class="row">
  <div class="col-md-12 text-center">
    <h1>CELULARES</h1>
  </div>
  <div class="container" style="background-color: white;">
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/c1.png" alt="1" width="100%"></tr>
          <tr><b>Xiaomi Redmi NOTE 11S 128GB 8GB</b></tr>
          <br>
          <tr>$305 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/c2.jpg" alt="2" width="100%"></tr>
          <tr><b>Xiaomi POCO M5S 4G 4GB/128GB</b></tr>
          <br>
          <tr>$250 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/c3.jpg" alt="3" width="100%"></tr><br>
          <tr><b>TECNO POVA 3 LF7 128GB+6GB BLACK</b></tr>
          <br>
          <tr>$275</tr>
        </td>
      </table>
    </div>
  </div>
</div>
<!-- 2 -->
<div class="row">
  <div class="container" style="background-color: white;">
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/c4.jpg" alt="4" width="100%"></tr>
          <tr><b>INFINIX HOT 20 X6826 6GB/128GB</b></tr>
          <br>
          <tr>$232 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/infinix.jpg" alt="5" width="100%"></tr>
          <tr><b>INFINIX NOTE 12 X670 8GB/256GB</b></tr>
          <br>
          <tr>$310 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/xiaomi-redmi.jpg" alt="6" width="100%"></tr>
          <tr><b>XIAOMI REDMI NOTE 11 //4GB-128GB</b></tr>
          <br>
          <tr>$258</tr>
        </td>
      </table>
    </div>
  </div>
</div>
