<!-- 1 -->
<div class="row">
  <div class="col-md-12 text-center">
    <h1>PRODUCTOS DE AUDIO</h1>
  </div>
  <div class="container" style="background-color: white;">
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/a1.jpg" alt="1" width="100%"></tr>
          <tr><b>Sonic 15″ 95000W</b></tr>
          <br>
          <tr>$212 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/a2a.jpg" alt="2" width="100%"></tr>
          <tr><b>TCL TS9030</b></tr>
          <br>
          <tr>$428 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/a3.jpg" alt="3" width="100%"></tr><br>
          <tr><b>LG SK1 40W</b></tr>
          <br>
          <tr>$71</tr>
        </td>
      </table>
    </div>
  </div>
</div>
<!-- 2 -->
<div class="row">
  <div class="container" style="background-color: white;">
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/a4.jpg" alt="4" width="100%"></tr>
          <tr><b>Klip Xtreme KFS500 WLSBT 3000W/b></tr>
          <br>
          <tr>$190 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/a5.jpg" alt="5" width="100%"></tr>
          <tr><b>Barra E350F</b></tr>
          <br>
          <tr>$40 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/a6.jpg" alt="6" width="100%"></tr>
          <tr><b>X-Boss 10″ XB-918 20000W</b></tr>
          <br>
          <tr>$130 </tr>
        </td>
      </table>
    </div>
  </div>
</div>
