<div class="col-md-12 text-center" style="background-color:#A4BC92">
  <h1><strong>IMPORTADORA</strong></h1>
  <h1><strong>"JavAsan"</strong></h1>
</div>
<div class="row">
  <div class="col-sm-8 col-sm-offset-2">
    <div id="myCarousel" class="carousel slide" data-ride="carousel" >
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="2" class="active"></li>
    		<li data-target="#myCarousel" data-slide-to="3" class="active"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active">
          <img src="<?php echo base_url(); ?>/assets/images/ban1.png" alt="1" width="100%">
        </div>

        <div class="item">
          <img src="<?php echo base_url(); ?>/assets/images/ban2.png"  alt="2" width="100%">
        </div>

        <div class="item">
          <img src="<?php echo base_url(); ?>/assets/images/ban3.png"  alt="3" width="100%">
        </div>

    		<div class="item">
          <img src="<?php echo base_url(); ?>/assets/images/ban4.png"  alt="4" width="100%">
        </div>
      </div>

      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
</div>
<br><br><br>
<!-- 1 -->
<div class="row">
  <div class="col-md-12 text-center" style="background-color:#A4BC92">
    <h1><strong>LO MÁS VENDIDO</strong></h1>
  </div>
  <div class="container" style="background-color:white">
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/asus.png" alt="1" height="100%" width="100%"></tr>
          <tr><b>Laptop ASUS VivoBook</b></tr>
          <br>
          <tr>$825 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><br><br><br></tr>
          <tr><img src="<?php echo base_url();?>assets/images/productos/dell.webp" alt="2" width="105%"></tr><br><br>
          <tr><b>Laptop Dell Vostro 14</b></tr>
          <br>
          <tr>$500 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/EPSON-L3250.jpg" alt="3" width="100%"></tr>
          <tr><b>EPSON L3250</b></tr>
          <br>
          <tr>$330</tr>
        </td>
      </table>
    </div>
  </div>
</div>
<!-- 2 -->
<div class="row">
  <div class="container" style="background-color:white">
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><br><br></tr>
          <tr><img src="<?php echo base_url();?>assets/images/productos/buds.webp" alt="4" width="280"></tr><br><br>
          <tr><b>Auriculares RedMi Buds Lite 3</b></tr>
          <br>
          <tr>$25 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/infinix.jpg" alt="5" width="280"></tr><br>
          <tr><b>INFINIX NOTE 12 X670 8GB/256GB</b></tr>
          <br>
          <tr>$310</tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/xiaomi-redmi.jpg" alt="6" width="280"></tr><br>
          <tr><b>XIAOMI REDMI NOTE 11 //4GB-128GB</b></tr>
          <br>
          <tr>$300 </tr>
        </td>
      </table>
    </div>
  </div>
</div>
<!-- 3 -->
<div class="row">
  <div class="container" style="background-color:white">
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/vape.png" alt="7" width="100"></tr><br>
          <tr><b>Vape Ultra 2500 </b></tr>
          <br>
          <tr>$7.00 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/HYUNDAI-HYLED32.jpg" alt="8" height="220px" width="250px"></tr><br>
          <tr><b>Hyundai 32″ HYLED3243NIM</b></tr>
          <br>
          <tr>$265 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table class="table table-striped table-hover table-responsive">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/productos/televisor-innova-4k.jpg" alt="Desayuno 9" height="220px" width="250px"></tr><br>
          <tr><b>INDURAMA 58″ 58TIKGF2UHD 4K SMART TV</b></tr>
          <br>
          <tr>$590</tr>
        </td>
      </table>
    </div>
  </div>
</div>
<hr>
