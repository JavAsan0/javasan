<div class="row">
  <div class="container" style="background-color:#7C9070;">
    <div class="col-md-3 text-center vertical-align:middle" >
      <img src="<?php echo base_url();?>assets/images/javasan.png" alt="Logos" height="100px">
    </div>
    <div class="col-md-9 text-Left">
      <h2><b>Importadora</b></h2>
      <h1><b>JavAsan</b></h1>
    </div>
  </div>
  <div class="container">
    <div class="col-md-6 text-center" style="background-color:#ABC4AA">
      <table>
        <td>
          <tr><h2><b>HISTORIA</b></h2></tr>
          <tr>
            <p style="text-align:justify;">“Importadora JavAsan es una empresa con más de 3 años de experiencia en manejo de tecnologias. Se formó en 2020 en la ciudad de Machala con la finalidad de solventar las necesidades de nuestros clientes y repuestos en hardware.
              <br>Hoy en día la empresa cuenta con sucursales en las ciudades de Latacunga, Machala, Guayaquil, Quito y Cuenca, gracias a la gran acogida que nuestros productos han tenido en el mercado ecuatoriano.
              <br>Seguiremos en nuestro afán de mejoramiento continuo del servicio que brindamos a la gente Ecuatoriana, satisfaciendo las necesidades con productos de calidad y servicio garantizado”.</p></tr>
        </td>
      </table>
    </div>
    <div class="col-md-6 text-center" style="background-color:#BBD6B8"> <!--C9DBB2-->
      <table>
        <td>
          <tr><h2><b>QUIENES SOMOS</b></h2></tr>
          <tr><p style="text-align:justify;">“Somos una importadora de tecnología. Vendemos productos de alta calidad y duración para poder servir a nuestros clientes. Ofrecemos tambien servicio técnico realizado en las 3 principales ciudades del país (Quito, Guayaquil y Cuenca) también brindamos servicios de mantenimiento preventivo y correctivo, con personal especializado que le brindara la asesoría necesaria. Todo con garantía del 100%.”.
          <br><br>¡Nos sentimos orgullosos de estar ahora más cerca de ti!</p></tr>
        </td>
        <br><br>
      </table>
    </div>
  </div>


  <div class="container" >
    <div class="col-md-4 text-center" style="background-color:#C7E9B0">
      <table border="1">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/mision.png" alt=""></tr>
          <tr><h2><b>MISIÓN</b></h2></tr>
          <tr><h5 style="text-align:justify;">“Proporcionar las tecnologías más innovadoras a medida de las necesidades empresariales, con el objetivo de incrementar su competitividad y productividad. Para ello implementamos soluciones prácticas adaptadas a sus necesidades y desarrollamos nuevas soluciones creativas. Nuestra base parte del aprovechamiento de las nuevas redes”.</h5></tr>
        </td>
        <br><br>
      </table>
    </div>
    <div class="col-md-4 text-center" style="background-color:#B3C99C"> <!--C9DBB2-->
      <table border="1">
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/vision.png" alt=""></tr>
          <tr><h2><b>VISIÓN</b></h2></tr>
          <tr><h5 style="text-align:justify;">“Ser la empresa mejor posicionada con presencia a nivel nacional en la comercialización de electrodomésticos, tecnología, línea de hogar y una alta gama de productos, contando con los mejores precios, estándares de calidad y un capital humano altamente capacitado, que brinde confianza y seguridad a nuestros clientes”.</h5></tr>
        </td>
        <br><br>
      </table>
    </div>
    <div class="col-md-4 text-center" style="background-color:#A4BC92">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/valores.png" alt=""></tr>
          <tr><h2><b>VALORES</b></h2></tr>
          <tr> Servicio</tr><br>
          <tr> Calidad</tr><br>
          <tr> Garantia</tr><br>
          <tr> Compromiso y honestidad</tr><br>
          <tr> Optimismo</tr><br>
          <tr> Mejora continua</tr><br>
        </td>
        <br><br>
      </table>
    </div>
  </div>
  <div class="col-md-12 text-center">
      <h5><strong>SÍGUENOS EN:</strong></h5>
      <a href="https://www.whatsapp.com/" target="_blank"> <img src=" <?php echo base_url();?>assets/images/w.ico" alt="whatsapp"></a>
      <a href="https://www.instagram.com/" target="_blank"> <img src="<?php echo base_url();?>assets/images/i.ico"></a>
      <a href="https://www.facebook.com/" target="_blank"> <img src=" <?php echo base_url();?>assets/images/f.ico"></a>
      <br><br>
  </div>
</div>
