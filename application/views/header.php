<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>JavAsan</title>

    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- IMPORTACION DE API KEY DE GOOGLE MAPS -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBcLcLBEAz0-5WQAt9Njj5kFlYuj-NUoSA&libraries=places&callback=initMap"></script>

  </head>
  <body style="background-color:#7C9070">
    <nav class="navbar  fixed-top navbar-light" style="background-color:#FEE8B0;"> <!--style="background-color: #146C94;"    navbar-expand-lg  -->
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <a class="navbar-brand" href="<?php echo site_url();?>"><img src="<?php echo base_url();?>assets/images/javasan.png" alt="Logo" height="180%" vertical-align:middle""></a>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <!-- Responsive -->
      <!-- <button class="navbar-toggler" type="button"  data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <svg xmlns="http://www.w3.org/2000/svg" height="3rem" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor">
              <path strokeLinecap="round" strokeLinejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
          </svg>
      </button> -->
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li><a href="<?php echo site_url();?>"style="color:black">Inicio</a></li>
          <li><a href="<?php echo site_url();?>/nosotros/nosotros" style="color:black">Nosotros</a></li>
          <!--<li><a href="<?php echo site_url();?>/tiendas/tienda" style="color:black">Tiendas</a></li>
          <li><a href="<?php echo site_url();?>/servicios/servicio" style="color:black">Servicio al cliente</a></li>
          <li><a href="<?php echo site_url();?>/contactos/contacto" style="color:black">Contactos</a></li> -->

          <li class="PROVEEDOR">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color:black" >PROVEEDORES <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo site_url();?>/proveedores/nuevo" style="color:black" >Nuevo Proveedor</a></li>
              <li><a href="<?php echo site_url();?>/proveedores/lista" style="color:black" >Listado</a></li>
              <!-- <li><a href="#">Something else here</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#">Separated link</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#">One more separated link</a></li> -->
            </ul>
          </li>

          <li class="PRODUCTOS">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color:black" >PRODUCTOS <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo site_url();?>/productos/nuevo" style="color:black">Nuevo Producto</a></li>
              <li><a href="<?php echo site_url();?>/productos/lista" style="color:black">Listado</a></li>
              <!-- <li><a href="#">Something else here</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#">Separated link</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#">One more separated link</a></li> -->
            </ul>
          </li>

          <li class="EMPLEADOS">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color:black" >EMPLEADOS<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo site_url();?>/empleados/nuevo" style="color:black" >Nuevo Empleado</a></li>
              <li><a href="<?php echo site_url();?>/empleados/lista" style="color:black" >Listado</a></li>
              <!-- <li><a href="#">Something else here</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#">Separated link</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#">One more separated link</a></li> -->
            </ul>
          </li>
        </ul>
    </div><!-- /.container-fluid -->
  </nav>
  </body>
</html>
