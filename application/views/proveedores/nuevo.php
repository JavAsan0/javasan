<h1>Nuevo Proveedor</h1>
<form class=""
action="<?php echo site_url(); ?>/proveedores/guardar"
method="post">
    <div class="row">
      <div class="col-md-6">
        <label for="">Nombre Proveedor:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el nombre del proveedor"
        class="form-control"
        name="nombre_prov" value=""
        id="nombre_prov">
      </div>
      <div class="col-md-6">
          <label for="">Ciudad:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la ciudad"
          class="form-control"
          name="ciudad_prov" value=""
          id="ciudad_prov">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-6">
          <label for="">Email:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el email"
          class="form-control"
          name="email_prov" value=""
          id="email_prov">
      </div>
      <div class="col-md-6">
          <label for="">Telefono:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el telefono"
          class="form-control"
          name="telefono_prov" value=""
          id="telefono_prov">
      </div>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/proveedores/lista"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
    <br>
    <br>
</form>
