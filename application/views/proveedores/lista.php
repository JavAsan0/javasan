div class="container">
  <div class="col-md-8">
      <h1>LISTADO DE PROVEEDORES</h1>
  </div>
  <div class="col-md-4">
    <br>
    <a href="<?php echo site_url('proveedores/nuevo');?>"class="btn btn-primary">
      <i class="glyphicon glyphicon-plus"></i>Agregar Proveedor</a>
  </div>
  <br>
<!-- Linea 13 del controlador proveedores -->
<?php if ($proveedores): ?>
  <table class="table table-striped table-bordered table-hover">
    <thead class="bg-danger">
      <tr>
        <th>ID</th>
        <th>NOMBRE PROVEEDOR</th>
        <th>CIUDAD</th>
        <th>EMAIL</th>
        <th>TELEFONO</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
  </thead>
  <tbody>
    <?php foreach ($proveedores as $filaTemporal): ?>
      <tr>
        <td><?php echo $filaTemporal->id_prov?></td>
        <td><?php echo $filaTemporal->nombre_prov?></td>
        <td><?php echo $filaTemporal->ciudad_prov?></td>
        <td><?php echo $filaTemporal->email_prov?></td>
        <td><?php echo $filaTemporal->telefono_prov?></td>
        <!-- Acciones -->
        <td class="text-center">
          <a href="#" title="Editar Instructor">
            <i class="glyphicon glyphicon-pencil"></i>
          </a>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <a href="<?php echo site_url("proveedores/eliminar/$filaTemporal->id_prov")?>"title="Eliminar Proveedor" onclick="return confirm ('¿Esta seguro?');" style="color:red">
           <i class="glyphicon glyphicon-trash"></i>
         </a>
       </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>
  <h1>No hay datos</h1>
<?php endif; ?>
