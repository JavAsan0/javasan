<div class="row">
  <div class="col-md-12 text-center">
    <h1>NUEVO PRODUCTO</h1>
  </div>
</div>
<form class=""
action="<?php echo site_url(); ?>/productos/guardar"
method="post">
    <div class="row">
      <div class="col-md-6">
        <label for="">Nombre Producto:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el nombre del producto"
        class="form-control"
        name="nombre_prod" value=""
        id="nombre_prod">
      </div>
      <div class="col-md-6">
          <label for="">Precio:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el precio del producto"
          class="form-control"
          name="precio_prod" value=""
          id="precio_prod">
      </div>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/productos/lista"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
    <br>
    <br>
</form>
