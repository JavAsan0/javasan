<div class="container">
  <div class="col-md-8">
      <h1>LISTADO DE PRODUCTOS</h1>
  </div>
  <div class="col-md-4">
    <br>
    <a href="<?php echo site_url('productos/nuevo');?>"class="btn btn-primary">
      <i class="glyphicon glyphicon-plus"></i>Agregar Producto</a>
  </div>
  <br>
<!-- Linea 13 del controlador productos -->
<?php if ($productos): ?>
  <table class="table table-striped table-bordered table-hover">
    <thead class="bg-danger">
      <tr>
        <th>ID</th>
        <th>NOMBRE DEL PRODUCTO</th>
        <th>PRECIO</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
  </thead>
  <tbody>
    <?php foreach ($productos as $filaTemporal): ?>
      <tr>
        <td><?php echo $filaTemporal->id_prod?></td>
        <td><?php echo $filaTemporal->nombre_prod?></td>
        <td><?php echo $filaTemporal->precio_prod?></td>
        <!-- Acciones -->
        <td class="text-center">
          <a href="#" title="Editar Instructor">
            <i class="glyphicon glyphicon-pencil"></i>
          </a>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <a href="<?php echo site_url("productos/eliminar/$filaTemporal->id_prod")?>"title="Eliminar Producto" onclick="return confirm ('¿Esta seguro?');" style="color:red">
           <i class="glyphicon glyphicon-trash"></i>
         </a>
       </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>
  <h1>No hay datos</h1>
<?php endif; ?>
