<div class="container">
  <div class="col-md-8">
      <h1>LISTADO DE EMPLEADOS</h1>
  </div>
  <div class="col-md-4">
    <br>
    <a href="<?php echo site_url('empleados/nuevo');?>"class="btn btn-primary">
      <i class="glyphicon glyphicon-plus"></i>Agregar Empleado</a>
  </div>
  <br>
<!-- Linea 13 del controlador proveedores -->
<?php if ($empleados): ?>
  <table class="table table-striped table-bordered table-hover">
    <thead class="bg-danger">
      <tr>
        <th>ID</th>
        <th>CEDULA</th>
        <th>NOMBRES</th>
        <th>APELLIDOS</th>
        <th>CARGO</th>
        <th>TELEFONO</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
  </thead>
  <tbody>
    <?php foreach ($empleados as $filaTemporal): ?>
      <tr>
        <td><?php echo $filaTemporal->id_emp?></td>
        <td><?php echo $filaTemporal->cedula_emp?></td>
        <td><?php echo $filaTemporal->nombres_emp?></td>
        <td><?php echo $filaTemporal->apellidos_emp?></td>
        <td><?php echo $filaTemporal->cargo_emp?></td>
        <td><?php echo $filaTemporal->telefono_emp?></td>
        <!-- Acciones -->
        <td class="text-center">
          <a href="#" title="Editar Empleado">
            <i class="glyphicon glyphicon-pencil"></i>
          </a>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <a href="<?php echo site_url("empleados/eliminar/$filaTemporal->id_emp")?>"title="Eliminar Empleado" onclick="return confirm ('¿Esta seguro?');" style="color:red">
           <i class="glyphicon glyphicon-trash"></i>
         </a>
       </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>
  <h1>No hay datos</h1>
<?php endif; ?>
