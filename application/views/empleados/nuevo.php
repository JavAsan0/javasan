<h1>Nuevo Empleado</h1>
<form class=""
action="<?php echo site_url(); ?>/empleados/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
        <label for="">Cedula:</label>
        <br>
        <input type="number"
        placeholder="Ingrese el numero de cedula"
        class="form-control"
        name="cedula_emp" value=""
        id="cedula_emp">
      </div>
      <div class="col-md-4">
        <label for="">Nombre Empleado:</label>
        <br>
        <input type="text"
        placeholder="Ingrese los nombres del empleado"
        class="form-control"
        name="nombres_emp" value=""
        id="nombres_emp">
      </div>
      <div class="col-md-4">
          <label for="">Apellidos Empleado:</label>
          <br>
          <input type="text"
          placeholder="Ingrese los apellidos del empleado"
          class="form-control"
          name="apellidos_emp" value=""
          id="apellidos_emp">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-6">
          <label for="">Cargo:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el cargo que desempeña"
          class="form-control"
          name="cargo_emp" value=""
          id="cargo_emp">
      </div>
      <div class="col-md-6">
          <label for="">Telefono:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el telefono"
          class="form-control"
          name="telefono_emp" value=""
          id="telefono_emp">
      </div>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/proveedores/lista"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
    <br>
    <br>
</form>
