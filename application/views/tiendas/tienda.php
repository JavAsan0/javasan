<div class="row">
  <div class="container" style="background-color:#7C9070;">
    <div class="col-md-3 text-center vertical-align:middle" >
      <img src="<?php echo base_url();?>assets/images/javasan.png" alt="Logos" height="100px">
    </div>
    <div class="col-md-9 text-Left">
      <h1><b>TIENDAS</b></h1>
    </div>
  </div>
  <div class="container">
    <div class="col-md-6 text-center" style="background-color:#ABC4AA">
      <table>
        <br>
        <td>
          <tr><img src="<?php echo base_url();?>/assets/images/direccion.ico" alt="Dir"><b>DIRECCIÓN SUCURSAL MACHALA</b>
          <br>
          Colon e/ Rocafuerte y, Bolivar, Av. 25 de Junio & , Machala 070102<br>
            Machala - Ecuador.</tr>
        </td>
        <br><br>
      </table>
    </div>
    <div class="col-md-6 text-center" style="background-color:#BBD6B8"> <!--C9DBB2-->
      <table>
        <br>
        <td>
          <tr><img src="<?php echo base_url();?>/assets/images/direccion.ico" alt="Dir"><b>DIRECCIÓN SUCURSAL LATACUNGA</b>
          <br>
            Av. Simón Rodríguez s/n Barrio El Ejido Sector San Felipe.<br>
            Latacunga - Ecuador.</tr>
        </td>
        <br><br>
      </table>
    </div>
  </div>


  <div class="container" >
    <div class="col-md-4 text-center" style="background-color:#C7E9B0">
      <table>
        <br>
        <td>
          <tr><img src="<?php echo base_url();?>/assets/images/direccion.ico" alt="Dir"><b>DIRECCIÓN SUCURSAL GUAYAQUIL</b>
          <br>
            V35R+983, Av Benjamín Carrión Mora, Guayaquil<br>
            Guayaquil - Ecuador.</tr>
        </td>
        <br><br>
      </table>
    </div>
    <div class="col-md-4 text-center" style="background-color:#B3C99C"> <!--C9DBB2-->
      <table>
        <br>
        <td>
          <tr><img src="<?php echo base_url();?>/assets/images/direccion.ico" alt="Dir"><b>DIRECCIÓN SUCURSAL QUITO</b>
          <br>
            Av. de los Shyris N32-218 y, Quito 170135.<br>
            Quito - Ecuador.</tr>
        </td>
        <br><br>
      </table>
    </div>
    <div class="col-md-4 text-center" style="background-color:#A4BC92">
      <table>
        <br>
        <td>
          <tr><img src="<?php echo base_url();?>/assets/images/direccion.ico" alt="Dir"><b>DIRECCIÓN SUCURSAL CUENCA</b>
          <br>
            Presidente Borrero 13-45, Cuenca<br>
            Cuenca - Ecuador.</tr>
        </td>
        <br><br>
      </table>
    </div>
  </div>
<br>
  <div class="row" >
    <div class="container" style="background-color:#7C9070; ">
      <div class="col-md-12 text-right" >
          <div id="mapaDireccion" style="width:100%; height: 270px;"></div>
      </div>
    </div>
  </div>
