<div class="row" style="color:black">
  <div class="container" style="background-color:#7C9070; ">
    <div class="col-md-3">
      <img src="<?php echo base_url();?>/assets/images/direccion.ico" alt="Dir"><b>  DIRECCIÓN SUCURSAL LATACUNGA</b>
      <br>
        Av. Simón Rodríguez s/n Barrio El Ejido Sector San Felipe.<br>
        Latacunga - Ecuador.
    </div>
    <div class="col-md-9 text-right" >
        <div id="mapaDireccion" style="width:100%; height: 250px;"></div>
    </div>
  </div>
</div>

<div class="row">
  <div class="container" style="background-color:#7C9070;">
    <div class="col-md-6 text-center">
      <h3><b>Contacto</b></h3>
      0999999999<br>
      (00)000-0000
    </div>
    <div class="col-md-6 text-center" >
      <h3><b>Horario</b></h3>
      lun:	07:00–17:30<br>
      mar:	07:00–17:30<br>
      mié:	07:30–17:30<br>
      jue:	07:30–17:30<br>
      vie:	07:30–16:30<br>
    </div>
  </div>
  <br>
</div>

  <div class="row"
  style="background-color:#FEE8B0; color:black; padding:20px">
    <div class="col-md-6 text-center">
      <img src="<?php echo base_url();?>assets/images/javasan.png" alt="Logo" height="80 px">
      <h5>
        Importadora JavAsan
      </h5>
    </div>
    <div class="col-md-6 text-center">
      <p>Terminos y condiciones</p>
      <p>Trabaja con nosotros</p>
      <p>Aceptamos todas tus tarjetas de crédito y débito</p>
      <img src="<?php echo base_url();?>assets/images/tarjetas.svg" alt="tarjetas" height="30 px">
    </div>
    <div class="col-md-12 text-center">
    </div>
  </div>
  <div class="row" style="background-color:black; color:white; padding:20px">
    <div class="col-md-12 text-center">
      <h5>Desarrollado por JavAsan Soft | Todos los derechos reservados &copy; 2023</h5>
    </div>
  </div>
</div>
<script type="text/javascript">
        function initMap(){
            // CREANDO EL PUNTO CENTRAL DEL MAPA
            // importar la latitud y longitus de google maps
            var coordenadaCentral = new google.maps.LatLng(-0.91764, -78.63303);
            //creando el mapa
            var mapa1=new google.maps.Map(
                document.getElementById('mapaDireccion'),
                {
                    center:coordenadaCentral,
                    zoom:15,
                    //ROADMAP para el camino puedo probar con terrine para ver terreno
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
            );
            var marcador1=new google.maps.Marker({
                  position: coordenadaCentral,
                  map:mapa1,
                  title:"JavAsan",
                  icon:"assets/images/m1.png",
                });
        }
</script>
</body>
</html>
