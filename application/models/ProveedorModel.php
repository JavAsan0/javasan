<?php
  class ProveedorModel extends CI_Model //CI_Model ya viene con el framework
  {
    function __construct()
    {
      // Reconocer a las clases
      parent::__construct();
    }
    //Funcion para insertar un instructor de MYSQL
    function insertar($datos){
      //Active Record en CodeIgniter
      return $this->db->insert("proveedor",$datos);

    }
    //Funcion para consultar Estudiantes
    function obtenerTodos(){
      $listadoProveedores=$this->db->get("proveedor"); //Devuelve un array   SIEMPRE VALIDAR CON UN IF
      if($listadoProveedores->num_rows()>0){ //SI HAY DATOS     num_rows nos deuelve el numero de filas que haya
        return $listadoProveedores->result();
      }else{ //NO HAY DATOS
        return false;
      }
    }

    function borrar($id_prov){
      $this->db->where("id_prov",$id_prov);
      if ($this->db->delete("proveedor")) {
        return true;
      } else {
        return false;
      }
    }
  }// Cierre de la clase
 ?>
