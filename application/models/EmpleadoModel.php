<?php
  class EmpleadoModel extends CI_Model //CI_Model ya viene con el framework
  {
    function __construct()
    {
      // Reconocer a las clases
      parent::__construct();
    }
    //Funcion para insertar un instructor de MYSQL
    function insertar($datos){
      //Active Record en CodeIgniter
      return $this->db->insert("empleado",$datos);

    }
    //Funcion para consultar Empleados
    function obtenerTodos(){
      $listadoEmpleados=$this->db->get("empleado"); //Devuelve un array   SIEMPRE VALIDAR CON UN IF
      if($listadoEmpleados->num_rows()>0){ //SI HAY DATOS     num_rows nos deuelve el numero de filas que haya
        return $listadoEmpleados->result();
      }else{ //NO HAY DATOS
        return false;
      }
    }

    function borrar($id_emp){
      $this->db->where("id_emp",$id_emp);
      if ($this->db->delete("empleado")) {
        return true;
      } else {
        return false;
      }
    }
  }// Cierre de la clase
 ?>
