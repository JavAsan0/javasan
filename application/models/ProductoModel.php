<?php
  class ProductoModel extends CI_Model //CI_Model ya viene con el framework
  {
    function __construct()
    {
      // Reconocer a las clases
      parent::__construct();
    }
    //Funcion para insertar un instructor de MYSQL
    function insertar($datos){
      //Active Record en CodeIgniter
      return $this->db->insert("producto",$datos);

    }
    //Funcion para consultar Estudiantes
    function obtenerTodos(){
      $listadoProductos=$this->db->get("producto"); //Devuelve un array   SIEMPRE VALIDAR CON UN IF
      if($listadoProductos->num_rows()>0){ //SI HAY DATOS     num_rows nos deuelve el numero de filas que haya
        return $listadoProductos->result();
      }else{ //NO HAY DATOS
        return false;
      }
    }

    function borrar($id_prod){
      $this->db->where("id_prod",$id_prod);
      if ($this->db->delete("producto")) {
        return true;
      } else {
        return false;
      }
    }
  }// Cierre de la clase
 ?>
