<?php
  class Productos extends CI_Controller
  {
    function __construct()
    {
      parent:: __construct();
      //error_reporting(0);

      //Cargar aqui todos los modelos
      $this->load-> model('ProductoModel');
    }
    public function lista(){
      $data['productos']=$this->ProductoModel->obtenerTodos();
      $this->load->view('header');
      $this->load->view('productos/lista',$data);
      $this->load->view('footer');
    }
    public function nuevo(){
      $this->load->view('header');
      $this->load->view('productos/nuevo');
      $this->load->view('footer');
    }

    public function guardar(){
      $datosNuevoProducto=array(
        "nombre_prod"=>$this->input->post('nombre_prod'),
        "precio_prod"=>$this->input->post('precio_prod')
      );
      if($this->ProductoModel->insertar($datosNuevoProducto)){
        redirect('productos/lista');
      }else{
        echo "<h1>ERROR AL INSERTAR</H1>"; //EMBEBER CODIGO
      }
    }

    //Funcion para eliminar estudiante
    public function eliminar($id_prod){
      if ($this->ProductoModel->borrar($id_prod)) {
        redirect('productos/lista');
      } else {
        echo "ERROR AL BORRAR :(";
      }
    }
  }//Cierre de la clase
 ?>
