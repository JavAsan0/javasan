<?php
  class Categorias extends CI_Controller
  {
    function __construct()
    {
      parent:: __construct();
    }
    public function audio(){
      $this->load->view('header');
      $this->load->view('categorias/audio');
      $this->load->view('footer');
    }
    public function celular(){
      $this->load->view('header');
      $this->load->view('categorias/celular');
      $this->load->view('footer');
    }
    public function computadora(){
      $this->load->view('header');
      $this->load->view('categorias/computadora');
      $this->load->view('footer');
    }
    public function televisor(){
      $this->load->view('header');
      $this->load->view('categorias/televisor');
      $this->load->view('footer');
    }
    public function electrodomestico(){
      $this->load->view('header');
      $this->load->view('categorias/electrodomestico');
      $this->load->view('footer');
    }
    public function domotica(){
      $this->load->view('header');
      $this->load->view('categorias/domotica');
      $this->load->view('footer');
    }
  }
 ?>
