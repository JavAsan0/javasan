<?php
  class Proveedores extends CI_Controller
  {
    function __construct()
    {
      parent:: __construct();
      //error_reporting(0);

      //Cargar aqui todos los modelos
      $this->load-> model('ProveedorModel');
    }
    public function lista(){
      $data['proveedores']=$this->ProveedorModel->obtenerTodos();
      $this->load->view('header');
      $this->load->view('proveedores/lista',$data);
      $this->load->view('footer');
    }
    public function nuevo(){
      $this->load->view('header');
      $this->load->view('proveedores/nuevo');
      $this->load->view('footer');
    }

    public function guardar(){
      $datosNuevoProveedor=array(
        "nombre_prov"=>$this->input->post('nombre_prov'),
        "ciudad_prov"=>$this->input->post('ciudad_prov'),
        "email_prov"=>$this->input->post('email_prov'),
        "telefono_prov"=>$this->input->post('telefono_prov')
      );
      if($this->ProveedorModel->insertar($datosNuevoProveedor)){
        redirect('proveedores/lista');
      }else{
        echo "<h1>ERROR AL INSERTAR</H1>"; //EMBEBER CODIGO
      }
    }

    //Funcion para eliminar estudiante
    public function eliminar($id_prov){
      if ($this->ProveedorModel->borrar($id_prov)) {
        redirect('proveedores/lista');
      } else {
        echo "ERROR AL BORRAR :(";
      }
    }
  }//Cierre de la clase
 ?>
