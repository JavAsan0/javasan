<?php
  class Empleados extends CI_Controller
  {
    function __construct()
    {
      parent:: __construct();
      //error_reporting(0);

      //Cargar aqui todos los modelos
      $this->load-> model('EmpleadoModel');
    }
    public function lista(){
      $data['empleados']=$this->EmpleadoModel->obtenerTodos();
      $this->load->view('header');
      $this->load->view('empleados/lista',$data);
      $this->load->view('footer');
    }
    public function nuevo(){
      $this->load->view('header');
      $this->load->view('empleados/nuevo');
      $this->load->view('footer');
    }

    public function guardar(){
      $datosNuevoEmpleado=array(
        "cedula_emp"=>$this->input->post('cedula_emp'),
        "nombres_emp"=>$this->input->post('nombres_emp'),
        "apellidos_emp"=>$this->input->post('apellidos_emp'),
        "cargo_emp"=>$this->input->post('cargo_emp'),
        "telefono_emp"=>$this->input->post('telefono_emp')
      );
      if($this->EmpleadoModel->insertar($datosNuevoEmpleado)){
        redirect('empleados/lista');
      }else{
        echo "<h1>ERROR AL INSERTAR</H1>"; //EMBEBER CODIGO
      }
    }

    //Funcion para eliminar estudiante
    public function eliminar($id_emp){
      if ($this->EmpleadoModel->borrar($id_emp)) {
        redirect('empleados/lista');
      } else {
        echo "ERROR AL BORRAR :(";
      }
    }
  }//Cierre de la clase
 ?>
